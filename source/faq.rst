Shotwell Frequently Asked Questions (FAQ)
#########################################

.. raw:: html

    <style> .blue {color:#488DD8} </style>
    <style> .nc {color:#7f7175} </style>

.. role:: blue
.. role:: nc

:blue:`Features`
----------------

Does Shotwell make permanent changes to my photos?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In general, **no**, but there are important exceptions. See `this page <http://www.yorba.org/shotwell/help/edit-nondestructive.html>`_ in our User's Manual.


Can I access a Shotwell library across a network, possibly from multilple machines?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is not recommended. Shotwell was not designed to support this use case, and the database can get into an inconsistent state. See `this SQLite page <http://www.sqlite.org/whentouse.html>`_ on network access of a database file, in particular the problem of file sharing protocols not properly locking the database which can cause corruption.


`Ticket #715764 <https://bugzilla.gnome.org/show_bug.cgi?id=715764>`_ tracks progress on making library network synchronization available in the future.


How can I move my photo files from one directory (or hard drive) to another?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Do not do this if you have RAW photos in your library**

There are two ways this can be done:

If you're moving all of your photos from your current library directory and you want all your future new photos to be imported to the new directory or hard drive, do the following:

* First, make sure you're running **Shotwell 0.8.1 or greater**. Earlier versions of Shotwell don't support library monitoring, which is required for the following to work.
* Quit Shotwell. Move your photos to the new directory or hard drive. Then start Shotwell. You'll see all your old photos go into the “Missing File” bin.
* Choose **Edit -> Preferences**. In the Preferences dialog you can select the library directory. Choose the directory where all your photos now reside. Then press the Close button.
* It might take a minute (depending on the size of your library) but Shotwell should find all the photos and associate the objects in its database with them.

If you're only moving some of your photos but don't want to change your library directory, do the following:

* You'll need to be running **Shotwell 0.9.0** or greater. A bug in older versions of Shotwell (`ticket #714148 <https://bugzilla.gnome.org/show_bug.cgi?id=717148>`_) prevents the following from working. 


How can I copy my Shotwell library to a new computer?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you want to copy your Shotwell library to a different computer, it’s a simple three step process:

If you’re running Shotwell 0.12.3 or earlier, copy the ~/.shotwell directory. If you’re running Shotwell 0.13.0 or later, copy the ~/.local/share/shotwell directory.

Copy your photo library or “Pictures” directory (see note below.)

Start Shotwell on the new computer, open Edit -> Preferences and make sure the pictures directory is correct.

Make sure to copy all subfolders of both the Pictures directory and the ~/.shotwell directory (or the ~/.local/share/shotwell directory for Shotwell 0.13.0 or later).

Note: Shotwell does not require your photos to be in a single directory. If the absolute paths are the same on the source and destination computers, Shotwell will recognize photos outside of the photo library (“Pictures”) directory. If the destination computer’s absolute paths are not identical Shotwell will mark those photos as missing; the easiest solution is to place the missing photos into the photo library directory.


How does Shotwell rotate photos when they're exported?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note that Shotwell is a non-destructive photo editor, so rotating a photo does not change the file immediately. However, when Shotwell exports a photo (with **File -> Export or File -> Publish…**) it does rotate the photo. It can do this in one of two ways, depending on the circumstances.

If the photo has not been transformed in any other way (no crop, color, or red-eye transformations) and the photo is being exported at its original size, Shotwell will not physically rotate the photo. Rather, it will set the Orientation field in the photo’s EXIF metadata header. This field is widely recognized by most software. It indicates which way the photo should be oriented when presenting it to the user (that is, the software will rotate it on-the-fly). By using this field, Shotwell avoids having to decode, rotate, and re-encode the photo. In the case of JPEG, where each re-encode is lossy, this prevents loss of detail and crispness.

If, however, the photo has been transformed in some way or it's being exported at a reduced size, Shotwell has no choice but to re-encode the photo. It uses that opportunity to rotate the pixels as well.


Where are your Online Service Privacy Policies?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

They are located `here <http://shotwell-project.org/doc/html/privacy-policy.html>`_.


:blue:`Bugs`
------------


How can I search for an existing ticket about a particular topic?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to https://bugzilla.gnome.org/browse.cgi?product=shotwell or https://gitlab.gnome.org/GNOME/shotwell/issues


I found a bug in Shotwell. How can I report it?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Plase see `Reporting a Bug <https://wiki.gnome.org/Apps/Shotwell/ReportingABug>`_ for more information.


I just imported a RAW photo into Shotwell and it looks overexposed or underexposed, why is this and how can I fix it?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Shotwell renders RAW images by picking some default tone mapping curves that work in most cases, but not all. If you shoot RAW+JPEG or even just plain RAW, your camera probably produces its own JPEG development of your RAW photo at exposure time, either as an associated JPEG file (in the RAW+JPEG case) or embedded in the RAW file itself (in the plain RAW) case. Since your camera presumably knows more about its CCD and the lighting conditions under which your photo was taken than Shotwell does, it’s development will likely look better than Shotwell’s. To switch your RAW Developer to your camera, if available, open the image you want to work with in single-photo mode by double-clicking on it. Then, under the “Developer” submenu of the “Photo” menu choose “Camera.” Due to a known issue with Shotwell 0.11.x and 0.12.x, if “Camera” is already selected you might have to first switch the developer to “Shotwell” and then back to “Camera” again to force the change to take effect.


I’m importing thousands of photos and Shotwell is using a huge amount of memory. Why?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This was `a known bug in Shotwell 0.7 <https://bugzilla.gnome.org/show_bug.cgi?id=716840>`_. Upgrading to Shotwell 0.8 or later should solve the problem.


When I'm viewing a lot of photos quickly, or Shotwell is renegerating a lot of thumbnails, Shotwell takes up a huge amount of memory. Why?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This was `a know bug <https://bugzilla.gnome.org/show_bug.cgi?id=715198>`_ existing prior to Shotwell 0.20.1. Upgrading to Shotwell 0.20.1 or later should solve the problem.


Why do my published photos not contain any geolocation information?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, both Flickr and Picasa Web Albums remove geolocation information from uploaded photos for security reasons. To use geolocation information with these services, you have to explicitly enable gelocation support.

For Picasa Web Albums, do the following:
* Log into your Picasa Web Albums account.
* In the upper-right hand corner of the welcome page, click “Settings” then choose “Photos settings” from the drop down menu.
* Click the “Privacy and Permissions” tab.
* Ensure that the “Automatically map photos if they contain location data” checkbox in the “Location” section is checked. 

For Flickr, follow these steps:
* Log into your Flickr account.
* Click “You” in the menu bar that runs across the top of the page, then choose “Your Account” from the drop-down menu.

* On the account options page, click the “Privacy & Permissions” tab.
* Scroll down to the “Defaults for new uploads” section.
* Ensure that the “Import EXIF location data” configuration option is set to “Yes.” 


My mouse cursor is hanging every second or so when Shotwell is running. Why?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This appears to be a problem with VirtualBox and USB mice. The full (closed-source) edition of VirtualBox passes USB devices on to virtual machines, and some users running this edition have reported this mouse cursor problem. Switching to the open-source edition of VirtualBox may help. See the comments at https://bugs.launchpad.net/ubuntu/+source/shotwell/+bug/555408.


Why doesn’t Shotwell update images after they’ve been edited by an external RAW editor?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you open a file in an external RAW editor and save the changes as a new JPEG file, Shotwell will not see or import the JPEG. You can import it manually, or check the Watch library directory for new files option in Preferences, which will cause Shotwell to automatically import new JPEG files from the library directory. However, the new JPEG file won’t be associated with the original RAW file.


Something happened and now all my photos are missing, help!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Assuming you didn’t delete (or unmount) your pictures folder, there’s a good chance you simply created a search filter and forgot about it. To see if this is the case, disable the search bar by hitting F8 or going to **View -> Search Bar**.

Note that the search bar is not persistent between Shotwell sessions in Shotwell 0.11 and above.


When I run Shotwell I see funny messages on the console.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You might see messages like these on the console when you run Shotwell:

``Error: Directory Canon: Next pointer is out of bounds; ignored``

``Warning: Directory Canon, entry 0x0100 has unknown Exif (TIFF) type 768; setting type size 1.``

``Error: Offset of directory NikonPreview, entry 0x0002 is out of bounds``

There are others, but these are typical examples.

These messages are harmless. They come from `Exiv2 <http://www.exiv2.org/>`_, the metadata library Shotwell uses to read EXIF/IPTC/XMP data from your photo files. Older versions of Exiv2 printed these messages whenever it encountered something odd or unexpected in the metadata it was parsing. There’s nothing wrong with your photos; these message give clues to how Exiv2 is interpreting them. However, there was no way to control their output until Exiv 0.21.

You can safely ignore these messages. However, if they bother you, you can upgrade to `Exiv 0.21.1 <http://www.exiv2.org/download.html>`_ and `GExiv2 0.3.1 <https://wiki.gnome.org/gexiv2>`_. The messages are then rerouted to Shotwell's log file.


Shotwell doesn’t seem to be able to connect to my Fuji or Nikon camera. Why?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This problem results from an interaction between some Fuji and Nikon models (most notably the Coolpix series) and `gPhoto <http://www.gphoto.org/>`_, a common Linux library Shotwell uses to access digital cameras. More information about this problem is available through gPhoto’s bug tracker issues

* http://sourceforge.net/tracker/index.php?func=detail&aid=1072668&group_id=8874&atid=108874

* http://sourceforge.net/tracker/index.php?func=detail&aid=3018517&group_id=8874&atid=108874

* http://sourceforge.net/tracker/index.php?func=detail&aid=3468856&group_id=8874&atid=108874

While there isn’t yet a solution to this problem, there is a workaround: simply remove the memory card from the camera and connect it to your computer through a card reader.


I disabled startup scan but Shotwell still scans all my photos when I run it. Is this a bug?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

No. What’s disabled in **Edit -> Preferences** is “Watch library directory for new photos”. When enabled, Shotwell will auto-import new photo and video files when they’re added to your Library directory. However, even if disabled, Shotwell will still scan your disk. Why?

Shotwell scans your library to ensure that all your existing photos and videos are still in place. If they’ve been renamed or moved, Shotwell can detect this and update its internal library. If they’ve been deleted, Shotwell moves those items into “Missing Photos”.

To skip scanning and monitoring existing items entirely, run Shotwell from the console:

``$ shotwell --no-runtime-monitoring``

We’re considering making this option available in Preferences (`ticket #717149 <https://bugzilla.gnome.org/show_bug.cgi?id=717149>`_).


:blue:`Backup`
--------------


How can I back up my Shotwell library?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are two issues here: backing up your photos and backing up the Shotwell database.

Backing up photos is beyond the scope of this question due to the size and quantity of the files. Wherever you decide to back up your photos, if you need to restore them, it's best if they are restored to the exact location they were in originally so Shotwell can locate them. Runtime monitoring in Shotwell (available in 0.8) will do its best to locate files that have moved, but it's simply easiest to restore your photos to the place they came from.

Backing up the Shotwell database is easier. However, the location of the database changed at version 0.13.


:nc:`For Shotwell versions 0.12 and earlier`


By default, Shotwell creates its data directory in ~/.shotwell (where the tilde represents your home directory). Inside this directory is the database file, thumbnails, and other files that Shotwell relies on for operation. If you have a large number of RAW files it can be large due to Shotwell generating mimics (JPEG files which can be used in place of decoding the RAW file).

To archive all these files, type this command in a console window:

``$ cd
$ tar -cpzf shotwell-backup.tar.gz ~/.shotwell``

This will create a shotwell-backup.tar.gz file in your home directory. If you run Shotwell from a different data directory (using the -d command-line option), replace the ~/.shotwell with your custom directory.

This command will create a single file holding the entire contents of your Shotwell data directory. If you want to restore this backup (make sure it's in your home directory), run this command:

``$ rm -rf ~/.shotwell
$ pushd /; tar -xpzf ~/shotwell-backup.tar.gz; popd``

Note that this will destroy your existing Shotwell library directory, so use with care.


:nc:`For Shotwell 0.13 and later`


Shotwell 0.13 and later stores the thumbnails in a separate location than the database, and additionally will regenerate them if they’re deleted, so they don’t need to be backed up. To backup the database:

``$ cd
$ tar -cpzf shotwell-backup.tar.gz ~/.local/share/shotwell``

This command will create a single file holding the entire contents of your Shotwell data directory. If you want to restore this backup (make sure it's in your home directory), run this command:

``$ rm -rf ~/.local/share/shotwell
$ pushd /; tar -xpzf ~/shotwell-backup.tar.gz; popd``

Note that this will destroy your existing Shotwell library directory, so use with care. 


:nc:`For custom library directories (-d command-line option)`


The above is for backing up Shotwell from the default data location. If you’re using a custom location for the database, you’ll need to modify the above steps to backup that location.


:blue:`Version`
^^^^^^^^^^^^^^^


I upgraded to Shotwell 0.14.x and now I get bad video thumbnails. This used to work. What gives?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Shotwell uses `GStreamer <http://gstreamer.freedesktop.org/>`_, the standard Linux framework for multimedia support, to obtain video thumbnails. Shotwell 0.13.1 and earlier were based on GStreamer 0.10, whereas Shotwell 0.14 uses GStreamer 1.0. These two versions of GStreamer are substantially different, and they have different packages of plugins that determine what video formats they support. In general, GStreamer 1.0 is a step forward over GStreamer 0.10, but there are some exceptions, mostly having to do with patent-encumbered video formats. As a result, Shotwell 0.14 may not be able to generate thumbnails for certain videos that Shotwell 0.13.1 could. Furthermore, video thumbnails generated by Shotwell 0.14 may have visual problems like ghosting or artifacting. Many of these problems will disappear as GStreamer 1.0 evolves. If you encounter a situation in Shotwell 0.14 generates a bad video thumbnail, please report a bug about it by following the steps above.


Can I run Shotwell 0.11 on Ubuntu 10.10 (Maverick Meerkat)?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Shotwell 0.11 is not officially supported on Ubuntu 10.10, but a community member has packaged it here: https://launchpad.net/~flexiondotorg/+archive/shotwell.


Can I run Shotwell 0.9.3 on Ubuntu 10.04 (Lucid Lynx)?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Shotwell 0.9.3 is not officially supported on Ubuntu 10.04, but a community member has https://launchpad.net/~flexiondotorg/+archive/shotwell packaged it here.


Why doesn’t the latest version of Shotwell run on my Ubuntu Long Term Support (LTS) release (for example, Lucid Lynx)?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Shotwell is under constant development to add new features and support for new technologies. Because of this, Shotwell often requires the latest libraries and desktop environment to build and run.

In contrast, Ubuntu’s Long Term Support (LTS) releases are **not** designed to be continually updated with the latest technologies. As the Ubuntu Wiki explains, an LTS is **not**:

* **Feature-Based Release:** We will focus on hardening functionality of existing features, versus introducing new ones.

* **Cutting Edge:** The benefit we gain from not introducing new bugs and/or regressions outweighs the new features and/or fixes. 


In short, an LTS is not intended to be updated with all the latest software as time goes on. In fact, an LTS is designed for the opposite, to be stocked with long-tested software and only upgrading them with critical and security-related fixes.

