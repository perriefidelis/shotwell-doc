.. Shotwell Documentation documentation master file, created by
   sphinx-quickstart on Sat Oct 16 16:44:19 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Shotwell
========

.. image:: ../images/Shotwell.png

Shotwell is a personal photomanager.


.. raw:: html

    <style> .blue {color:#488DD8} </style>
    <style> .strike {text-decoration:line-through} </style>

.. role:: blue
.. role:: strike

:blue:`News`
------------

* **Aug 18**:superscript:`th` **2021:** `Shotwell <https://flathub.org/repo/appstream/org.gnome.Shotwell.flatpakref>`_ 0.30.14 now available `(Sources) <https://download.gnome.org/sources/shotwell/0.30/shotwell-0.30.14.tar.xz>`_

* **Dec 22**:superscript:`nd` **2020:** Shotwell 0.31.3 now available `(Sources) <https://download.gnome.org/sources/shotwell/0.30/shotwell-0.31.3.tar.xz>`_


:blue:`Features`
----------------
* Import from disk or camera
* Organize by time-based Events, Tags(keywords), Folders, and more
* View your photos in full-window or fullscreen mode
* Crop, rotate, color adjust, straighten, and enhance photos
* Slideshow
* Video and RAW photo support
* Share to major Web services, including :strike:`Facebook`, Flickr, Tumblr and YouTube



:blue:`Screenshots`
-------------------

.. |logo1| image:: ../images/harbor.png
   :width: 400px
   :height: 200px
   :align: middle
.. |logo2| image:: ../images/import.png
   :width: 400px
   :height: 200px
   :align: middle
.. |logo3| image:: ../images/organize.png
   :width: 400px
   :height: 200px
   :align: middle
.. |logo4| image:: ../images/edit.png
   :width: 400px
   :height: 200px
   :align: middle
.. |logo5| image:: ../images/publish.png
   :width: 330px
   :height: 200px
   :align: middle

======= =======
|logo1| |logo2|
======= =======
|logo3| |logo4|
======= =======
|logo5|
=======


:blue:`Documentation`
---------------------
* `User's Guide <http://shotwell-project.org/doc/html>`_
* :doc:`faq`
* `Tips for reporting a Shotwell bug <https://wiki.gnome.org/Apps/Shotwell/ReportingABug>`_
* `Building and Installing <https://wiki.gnome.org/Apps/Shotwell/BuildingAndInstalling>`_


:blue:`Privacy policies for various services`
---------------------------------------------
* `Privacy Policy for Google Photos, YouTube and Flickr <http://shotwell-project.org/doc/html/privacy-policy.html>`_


:blue:`Getting in Touch`
------------------------
* `IRC Channel <irc://irc.gnome.org/%23shotwell>`_
* `Using GNOME's Discourse <https://discourse.gnome.org/tag/shotwell>`_
* `Search open bugs <https://gitlab.gnome.org/GNOME/shotwell/issues>`_
* `File a bug <https://gitlab.gnome.org/GNOME/shotwell/issues/new>`_
* `"I use Shotwell" Flickr group <http://www.flickr.com/groups/shotwell>`_


:blue:`Development Resources`
-----------------------------
* Git: ``git clone https://gitlab.gnome.org/GNOME/shotwell.git``
* `Browse source <https://gitlab.gnome.org/GNOME/shotwell>`_
* `Tarballs <https://download.gnome.org/sources/shotwell/>`_
* `Release history <https://gitlab.gnome.org/GNOME/shotwell/blob/master/NEWS>`_


:blue:`More information about developing or contributing to Shotwell:`
----------------------------------------------------------------------
* `Shotwell architecture overview <https://wiki.gnome.org/Apps/Shotwell/Architecture>`_
* `Shotwell coding conventions <https://wiki.gnome.org/Apps/Shotwell/CodingConventions>`_
* `Translation and internationalization <https://l10n.gnome.org/module/shotwell/>`_
    * `How to start using GNOME's translation system <http://mavridou.blogspot.com/2014/05/how-to-start-using-gnomes-translation_22.html>`_
* `Shotwell coding notes <https://wiki.gnome.org/Apps/Shotwell/CodingNotes>`_
* `Guide for writing Shotwell plugins <https://wiki.gnome.org/Apps/Shotwell/Architecture/WritingPlugins>`_
* Miscellanea:
    * `Shotwell developer's jot sheet <https://wiki.gnome.org/Apps/Shotwell/JotSheet>`_
    * `Metadata and tags in other applications <https://wiki.gnome.org/Apps/Shotwell/PhotoTags>`_
    * `Hierarchial tags in other applications <https://wiki.gnome.org/Apps/Shotwell/HierarchicalTags>`_
    * `Notes on the Faces branch <https://wiki.gnome.org/Apps/Shotwell/FacesTool>`_
    * `Planning/Notes on the upcoming Geolocation and map view features <https://wiki.gnome.org/Apps/Shotwell/Location>`_
    * `Checklist for making a release <https://wiki.gnome.org/Apps/Shotwell/MakingARelease>`_
